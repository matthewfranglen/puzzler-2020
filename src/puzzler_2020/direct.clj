(ns puzzler-2020.direct)

(defn code [bitmap]
  (list 'let ['bitmap (list 'int-array (vec bitmap))]
        '(do
           (defn to-idx
             [letter]
             (- (int letter) (int \a)))
           (defn to-mask
             [letter]
             (bit-shift-left 1 (to-idx letter)))
           (defn next-char-map
             [letter]
             (let [idx (to-idx letter)]
               (if (<= 0 idx 25) (aget bitmap idx) 0)))
           (defn is-in?
             [letter bitmap]
             (not= (bit-and (to-mask letter) bitmap) 0))
           (defn rest-valid?
             [word length]
             (or (<= length 1)
                 (and (is-in? (second word) (next-char-map (first word)))
                      (rest-valid? (rest word) (dec length)))))
           (defn is-valid?
             [word]
             (let [length (count word)
                   first-map (next-char-map (first word))]
               (if (= length 1)
                 (not= first-map 0)
                 (and (not= first-map 0)
                      (is-in? (nth word 1) first-map)
                      (rest-valid? (rest word) (dec length))))))

           (defn -main
             [file & args]
             (doseq [word (->> file
                               clojure.java.io/reader
                               line-seq)]
               (let [lower-word (clojure.string/lower-case word)]
                 (if (is-valid? lower-word)
                   (println word))))))))
