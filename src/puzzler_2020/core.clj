(ns puzzler-2020.core
  (:gen-class))

(declare generate-bitmap)
(declare apply-filter)
(declare connexions)
(declare bidirectionalize)
(declare to-map)
(declare to-bitmap)
(declare is-valid?)
(declare read-file)

(defn -main
  "Read the file and filter the lines by the graph"
  [letters file & args]
  (if (not= (count letters) 13)
    (println "You must supply 13 letters to form the graph")
    (doseq [word (apply-filter (generate-bitmap letters) file)]
      (println word))))

(defn generate-bitmap
  "Generate the bitmap from the letters"
  [letters]
  (->> letters connexions bidirectionalize to-map to-bitmap))

(defn apply-filter
  "Read the file and apply the bitmap filter"
  [bitmap file]
  (->> file read-file (map clojure.string/lower-case) (filter #(is-valid? bitmap %))))

(defn connexions
  "Convert the list of letters into the letter pairs.
  The letters are provided as a list, e.g. brchsytlegwno.
  They are then split into the two circles (O and M for outer and middle) and the center (C): brchsy / tlegwn / o.
  Each circle connects as a circular double linked list:
    O(i) -> O(i + 1)
    M(i) -> M(i + 1)
    O(i) -> M(i)
    O(i) -> M(i + 1)
    O(i) -> C
    M(i) -> C
  All O and All M conecct to the center letter.
  Connections are bi directional.

  This returns the connections defined above, further work must be done to create the bi directionality and the sorting.
  "
  [letters]
  (letfn [(rotate [letters] (take (count letters) (cons (last letters) letters)))
          (conn-circle [letters] (map vector (rotate letters) letters))
          (conn-outer-middle [outer middle] (concat (map vector outer middle) (map vector (rotate outer) middle)))
          (conn-all-inner [circle inner] (map #(vector % inner) circle))]
    (let [outer (take 6 letters)
          middle (->> letters (drop 6) (take 6))
          inner (last letters)]
      (concat (conn-circle outer)
              (conn-circle middle)
              (conn-outer-middle outer middle)
              (conn-all-inner outer inner)
              (conn-all-inner middle inner)))))

(defn bidirectionalize
  "Make unidirectional connections bidirectional.
  This is just a matter of repeating the connection in reverse.
  This returns a set to ensure that the entries are unique.
  "
  [pairs]
  (->> pairs (map reverse) (map vec) (concat pairs) set))

(defn to-map
  "Make a hash map of the directional pairs.
  The keys are the first letter, the values are lists of sorted subsequent letters.
  There is probably a much much easier way to do this."
  [pairs]
  (let [sorted-pairs (sort pairs)
        grouped-pairs (group-by first sorted-pairs)
        pair-first (keys grouped-pairs)
        pair-seconds (->> (vals grouped-pairs) (map #(map second %)) (map #(->> % set sort)))]
    (zipmap pair-first pair-seconds)))

(defn to-bitmap
  "Make a bitmap of the adjacency matrix.
  This makes hard assumptions about the inputs:
    * input must be ascii, all letters out of that range are invalid already
    * input must be lower case
  This means that there are 26 possible letters.
  A Java Integer is 4 bytes, which is 32 bits, so an integer can be an adjacency map for a single letter.
  This means that the overall data structure is int[26].

  This requires bidirectional pairs.
  "
  [prefix-map]
  (letfn [(char-range [start end] (map char (range (int start) (inc (int end)))))
          (to-idx [letter] (- (int letter) (int \a)))
          (to-int [letter] (bit-shift-left 1 (to-idx letter)))
          (to-adjacency [letters] (reduce + (map to-int letters)))]
    (->> (char-range \a \z) (map #(get prefix-map % [])) (map to-adjacency) int-array)))

(defn is-valid?
  "Check if the provided word passes the bitmap filter."
  [bitmap word]
  (let [length (count word)]
    (letfn [(to-idx [letter] (- (int letter) (int \a)))
            (to-mask [letter] (bit-shift-left 1 (to-idx letter)))
            (next-char-map [letter] (let [idx (to-idx letter)]
                                      (if (<= 0 idx 25) (aget bitmap idx) 0)))
            (is-in? [letter bitmap] (not= (bit-and (to-mask letter) bitmap) 0))
            (rest-valid? [word length]
              (let [char-map (next-char-map (first word))]
                (if (<= length 1)
                  true
                  (if (is-in? (second word) char-map)
                    (rest-valid? (rest word) (dec length))
                    false))))]
      (let [first-map (next-char-map (first word))]
        (if (= length 1)
          (not= first-map 0)
          (if (and (not= first-map 0) (is-in? (nth word 1) first-map))
            (rest-valid? (rest word) (dec length))
            false))))))

(defn read-file
  "Read the file as a lazy sequence"
  [file]
  (line-seq (clojure.java.io/reader file)))
