(ns puzzler-2020.transform
  (:gen-class)
  (:require [puzzler-2020.direct :refer [code]]))

(defn transform
  "Transforms the code into code that only uses the words.
  Creates macros that will convert the code back.

  The principle behind this is simple.
  Each invocation or literal declaration is replaced with a single word.
  The word will reform the structure of it's arguments as well as replacing itself with the original.

  So given the code:
  (println (str (inc 1) 23))
  And the words:
  a b c d e f g
  This becomes:
  (a b c)
  Where a is:
  [& args] -> (println (args)) -> (println (b c))
  Where b is:
  [& args] -> (str (args) 23) -> (str (c) 23)
  Where c is:
  [& args] -> (inc 1) -> (inc 1)

  This technique requires that there be at least as many words as there are units of replacement.
  This does not produce reusable units."
  [code words]
  (letfn [(filter-words [words]
            (let [unique-words (-> words set sort)
                  existing-function-names (->> (ns-publics 'clojure.core) keys (map str) set)]
              (->> unique-words (filter #(not (contains? existing-function-names %))))))
          (transform-recurse [code words]
            (let [function (first code)
                  args (rest code)
                  word (symbol (first words))
                  remainder (rest words)]
              (if (empty? args)
                {:code (list word)
                 :macro (list 'defmacro word '[& args] (list 'quote function))
                 :words remainder}
                (let [transformed (reduce arg-recurse {:words remainder} args)
                      macro (list 'defmacro word '[& args] (list 'quote (conj (:post-code transformed) function)))]
                  {:code (conj (:pre-code transformed) word)
                   :macro (conj (:macro transformed) macro)
                   :words (:words transformed)}))))
          (arg-recurse [acc value]
            (if (list? value)
              (list-recurse acc value)
              (if (vector? value)
                (vec-recurse acc value)
                (atom-recurse acc value))))
          (list-recurse [acc value]
            (let [result (transform-recurse value (:words acc))]
              {:pre-code (concat (:pre-code acc) (:code result))
               :post-code (concat (:post-code acc) (-> result :code list))
               :macro (concat (:macro acc) (:macro result))
               :words (:words result)}))
          (vec-recurse [acc value]
            (let [result (reduce arg-recurse {:words (:words acc)} value)]
              {:pre-code (concat (:pre-code result) (:pre-code acc))
               :post-code (concat (:post-code acc) [(-> result :post-code vec)])
               :macro (concat (:macro acc) (:macro result))
               :words (:words result)}))
          (atom-recurse [acc value]
            (assoc acc :post-code (concat (:post-code acc) (list value))))]
    (let [result (transform-recurse code (filter-words words))]
      {:code (:code result) :macro (:macro result)})))
