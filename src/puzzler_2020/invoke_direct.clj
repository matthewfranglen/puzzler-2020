(ns puzzler-2020.invoke_direct
  (:gen-class)
  (:require [puzzler-2020.direct :refer [code]]))

(defmacro brown "Replaces itself with code"
  [& body]
  (code [0
         17451008
         147664
         0
         149572
         0
         4210836
         4472900
         0
         0
         0
         671762
         0
         21774336
         21899478
         0
         0
         18454
         20996224
         16803842
         0
         0
         286912
         0
         811010
         0]))

(brown)
