(ns puzzler-2020.generate
  (:gen-class)
  (:require [puzzler-2020.core]
            [puzzler-2020.direct]
            [puzzler-2020.transform]))

(declare generate)

(defn -main
  "Generate the source code for a specific version"
  [letters file & args]
  (if (not= (count letters) 13)
    (println "You must supply 13 letters to form the graph")
    (generate letters file)))

(defn generate [letters dictionary-file]
  (let [bitmap (puzzler-2020.core/generate-bitmap letters)
        words (puzzler-2020.core/apply-filter bitmap dictionary-file)
        code (puzzler-2020.direct/code bitmap)
        generated (puzzler-2020.transform/transform code words)]
    (do
      (with-open [writer (clojure.java.io/writer (str "src/" letters "_macro.clj"))]
        (do (.write writer (str "(ns " letters "-macro)"))
            (.write writer (str (:macro generated)))))
      (with-open [writer (clojure.java.io/writer (str "src/" letters ".clj"))]
        (do (.write writer (str "(ns " letters " (:gen-class) (:require [" letters "-macro :refer :all]))"))
            (.write writer (str (:code generated))))))))
