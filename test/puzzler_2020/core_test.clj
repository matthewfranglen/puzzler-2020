(ns puzzler-2020.core-test
  (:require [clojure.test :refer :all]
            [puzzler-2020.core :refer :all]))

(deftest test-connexions
  (testing "Test basic connections"
    (let [letters "brchsytlegwno"
          actual (connexions letters)
          expected (vector
                    ;; outer circle
                    [\y \b]
                    [\b \r]
                    [\r \c]
                    [\c \h]
                    [\h \s]
                    [\s \y]
                    ;; middle circle
                    [\n \t]
                    [\t \l]
                    [\l \e]
                    [\e \g]
                    [\g \w]
                    [\w \n]
                    ;; outer to middle
                    [\b \t]
                    [\r \l]
                    [\c \e]
                    [\h \g]
                    [\s \w]
                    [\y \n]
                    [\y \t]
                    [\b \l]
                    [\r \e]
                    [\c \g]
                    [\h \w]
                    [\s \n]
                    ;; everything to inner
                    [\b \o]
                    [\r \o]
                    [\c \o]
                    [\h \o]
                    [\s \o]
                    [\y \o]
                    [\t \o]
                    [\l \o]
                    [\e \o]
                    [\g \o]
                    [\w \o]
                    [\n \o])]
      (is (= actual expected)))))

(deftest test-bidirectionalize
  (testing "Test generation of bidirectional pairs"
    (let [pairs
          [[\y \b]
           [\b \r]
           [\r \c]
           [\c \h]
           [\h \s]
           [\s \y]]
          actual (bidirectionalize pairs)
          expected #{[\y \b]
                     [\b \r]
                     [\r \c]
                     [\c \h]
                     [\h \s]
                     [\s \y]
                     [\b \y]
                     [\r \b]
                     [\c \r]
                     [\h \c]
                     [\s \h]
                     [\y \s]}]
      (is (= actual expected)))))

(deftest test-compose-bidirectionalize
  (testing "Test composition of connexions and bidirectionalize"
    (->> "brchsytlegwno" connexions bidirectionalize)))

(deftest test-to-map
  (testing "Test generation of prefix map"
    (let [pairs
          [[\a \c]
           [\a \b]
           [\b \d]]
          actual (to-map pairs)
          expected {\a (list \b \c), \b (list \d)}]
      (is (= actual expected)))))

(deftest test-compose-to-map
  (testing "Test composition of connexions to to-map"
    (->> "brchsytlegwno" connexions bidirectionalize to-map)))

(deftest test-to-bitmap
  (testing "Test generation of bitmap"
    (let [prefix-map {\a [\b \c]
                      \b [\d]
                      \y [\z]}
          actual (to-bitmap prefix-map)
          expected [6 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 33554432 0]]
      (is (= (vec actual) expected)))))

(deftest test-compose-to-bitmap
  (testing "Test composition of connexions to to-bitmap"
    (->> "brchsytlegwno" connexions bidirectionalize to-map to-bitmap)))

(deftest test-is-valid?
  (testing "Test is-valid? filter"
    (let [bitmap (->> "brchsytlegwno" connexions bidirectionalize to-map to-bitmap)
          word "brown"
          actual (is-valid? bitmap word)
          expected true]
      (is (= actual expected)))))
