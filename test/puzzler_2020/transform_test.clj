(ns puzzler-2020.transform-test
  (:require [clojure.test :refer :all]
            [puzzler-2020.transform :refer :all]
            [puzzler-2020.core]
            [puzzler-2020.generate]))

(deftest test-single-fn-transform
  (testing "Test most basic transform"
    (let [code '(list)
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a)
          expected-macros '(defmacro a [& args] 'list)]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-list-transform
  (testing "Test list transform"
    (let [code '(println (str (inc 1) 23))
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a b c)
          expected-macros '((defmacro a [& args] '(println (b c)))
                            (defmacro b [& args] '(str (c) 23))
                            (defmacro c [& args] '(inc 1)))]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-vec-transform
  (testing "Test vector transform"
    (let [code '(let [value 23] (println value))
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a b)
          expected-macros '((defmacro a [& args] '(let [value 23] (b)))
                            (defmacro b [& args] '(println value)))]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-vec-list-transform
  (testing "Test vector list transform"
    (let [code '(let [value (inc 1)] (println value))
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a b c)
          expected-macros '((defmacro a [& args] '(let [value (b)] (c)))
                            (defmacro b [& args] '(inc 1))
                            (defmacro c [& args] '(println value)))]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-vec-list-transform-2
  (testing "Test vector list transform"
    (let [code '(println (str value [1 2 3]))
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a b)
          expected-macros '((defmacro a [& args] '(println (b)))
                            (defmacro b [& args] '(str value [1 2 3])))]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-vec-list-transform-3
  (testing "Test vector list transform"
    (let [code '(println (conj [1 2 (inc 2)] value))
          words ["a" "b" "c" "d" "e" "f" "g"]
          actual (transform code words)
          actual-code (:code actual)
          actual-macros (:macro actual)
          expected-code '(a b c)
          expected-macros '((defmacro a [& args] '(println (b c)))
                            (defmacro b [& args] '(conj [1 2 (c)] value))
                            (defmacro c [& args] '(inc 2)))]
      (do (is (= actual-code expected-code))
          (is (= actual-macros expected-macros))))))

(deftest test-complete-transform
  (testing "Test transform of direct.code"
    (let [bitmap (puzzler-2020.core/generate-bitmap "brchsytlegwno")
          words (puzzler-2020.core/apply-filter bitmap "files/british-english")
          code (puzzler-2020.direct/code bitmap)
          actual (transform code words)])))
