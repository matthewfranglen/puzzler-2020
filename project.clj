(defproject puzzler-2020 "0.1.0-SNAPSHOT"
  :description "My submission for the 2020 Brandwatch Christmas Puzzler"
  :dependencies [[org.clojure/clojure "1.10.1"]]
  :main ^:skip-aot puzzler-2020.core
  :target-path "target/%s"
  :local-repo "dependencies"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})
