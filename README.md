Puzzler 2020
============

This is my submission for the 2020 Brandwatch Christmas Puzzler.

The puzzle is based around this graph:
![puzzler graph](media/puzzler.jpeg)

You can start at any point.
You can follow any connection.
You can return to letters and you can use connections more than once.
Letters are not connected to themselves.

The challenge is to write code that can determine the words that adhere to these rules.
The example wordlist is any dictionary from `/usr/share/dict/`.
I have chosen to use the `british-english` one, which is included in this repo.

It's a bit of fun.
The solution doesn't have to be super fast or perfectly written.
Maybe it is creative or unusual?

Solution
--------

I like to be more creative in my solutions.

This solution uses clojure.
The objective of my solution is to write code using only valid words to perform the filtering.

### Sane Approach

The basic approach is to generate an adjacency matrix.
This is done using an array of 26 integers.
Each integer is 32 bits so a 26x26 matrix can be formed out of this.
The rows are the first letter, the bits are the second letter.
As there can be single letter words, if the row has a value of zero it is not a valid single letter word.

All of this is fine, and you can view the basic implementation in [src/puzzler_2020/core.clj](src/puzzler_2020/core.clj).
If you wish to run this then invoke the following:

```bash
make build
java -jar target/uberjar/puzzler-2020-0.1.0-SNAPSHOT-standalone.jar brchsytlegwno files/british-english
```

The string `brchsytlegwno` is formed by converting the graph in a very specific way:

 * Loop the outer ring (`brchsy`)
 * Loop the inner ring (`tlegwn`)
 * The central letter (`o`)

The inner ring must start at the letter which is connected to the starting letter of the outer ring, and is the counter clockwise one of the two choices.

#### Note

The code will lowercase any input as part of processing.
This does result in the output being lower case.
If the input file has the same word with varied case then the output can have duplicate words.

### Almost Perfect Approach

Clojure is homoiconic, meaning that the code written in the language is encoded as data structures that the language has to manipulate.
This makes it very easy to write code that manipulates clojure code.

The almost perfect approach starts with a trimmed down version of the sane approach, which just works with a precomputed adjacency matrix.
This code is then transformed into a list of words that pass the filter, along with the macros that will convert that code back into the original code.
The macros run at compile time so the compiled code is equivalent to the pre-transform code.
The only code that does not pass the filter is the namespace declaration which imports the macros and triggers class generation.

Since this approach needs a word list, it uses the same dictionary that you use for the sane approach.
As it happens the sane approach code is used quite heavily when generating the almost perfect code.

You can see the almost perfect code in [src/brchsytlegwno.clj](src/brchsytlegwno.clj).

To generate the almost perfect code from scratch you must:

 * Build the basic code
 * Generate the almost perfect code
 * Build again to compile the almost perfect code

```bash
make build
make generate-brchsytlegwno
make build
```

You can then run it with `make run-brchsytlegwno` (review the makefile for the full command).
If you wish to change the dictionary then just set the `INPUT_FILE` variable:

```bash
INPUT_FILE=/usr/share/dict/american-english make run-brchsytlegwno
```

The makefile is setup to allow you to provide an arbitrary 13 letter graph, so you can use anything you want after `generate-` or `run-`.

### Example Code

The code in the generated file can be slightly hard to read.
A slightly reformatted version looks like:

```clojure
(ns brchsytlegwno
  (:gen-class)
  (:require [brchsytlegwno-macro :refer :all]))

(b blob   bloc   bloch   blog
   blot   blow   blown   blows
   blowsy bob    boer    bog
   bole   bolero boleros bolt
   bolton bono   bony    bore
   borer  boron  bosh    bow
   bows   boy    boys    bronson
   brow   brown  browns  brows
   by     byblos c       celt
   choler chore  chow    chows
   cob    coerce cog     colby
   cole   colon  colons  colony
   colt   con    console core
   corot)
```
