.PHONY: generate-brchsytlegwno generate-brchsytlegwro \
	run-brchsytlegwno run-brchsytlegwro \
	build test repl

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = puzzler-2020

LEIN_DOCKER_IMAGE ?= clojure:openjdk-8-lein
LEIN_COMMAND = docker run --rm --user $(shell id -u) --volume $(PWD):/project --volume $(HOME)/.lein:/.lein --workdir /project $(LEIN_DOCKER_IMAGE) lein
INPUT_FILE ?= files/british-english
DOCKER_DEPENDENCY = .make/docker

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Generate the ideal form of the code
generate-brchsytlegwno generate-brchsytlegwro : generate-% : target/uberjar/puzzler-2020-*-standalone.jar
	java -cp target/uberjar/puzzler-2020-*-standalone.jar puzzler_2020.generate $* $(INPUT_FILE)

generate-% : build
	java -cp target/uberjar/puzzler-2020-*-standalone.jar puzzler_2020.generate $* $(INPUT_FILE)

## Run the ideal form of the code
run-brchsytlegwno run-brchsytlegwro : run-% : target/uberjar/puzzler-2020-*-standalone.jar
	java -cp target/uberjar/puzzler-2020-*-standalone.jar $* $(INPUT_FILE)

run-% : target/uberjar/puzzler-2020-*-standalone.jar
	java -cp target/uberjar/puzzler-2020-*-standalone.jar $* $(INPUT_FILE)

## Build jar
build : $(DOCKER_IMAGE) test
	$(LEIN_COMMAND) uberjar

## Test code
test : $(DOCKER_IMAGE)
	$(LEIN_COMMAND) test

## Command line REPL, useful for integrating with vim fireplace.
## This runs locally because I assume you have lein set up if you want to work with this code.
repl :
	lein repl

## Run basic code with specific wordlist
/usr/share/dict/% : build
	java -jar target/uberjar/puzzler-2020-*-standalone.jar /usr/share/dict/$*

## Run basic code with specific wordlist
files/% : build
	java -jar target/uberjar/puzzler-2020-*-standalone.jar files/$*

target/uberjar/puzzler-2020-*-standalone.jar : $(DOCKER_IMAGE)
	$(LEIN_COMMAND) uberjar

$(DOCKER_IMAGE) :
	docker pull $(LEIN_DOCKER_IMAGE)
	if [ ! -e .make ]; then mkdir .make; fi
	touch $(DOCKER_IMAGE)

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')


